package com.aptech.pet_adoption.animal.controller;

import com.aptech.pet_adoption.animal.payload.request.DeleteAnimal;
import com.aptech.pet_adoption.animal.payload.request.UpdateAnimal;
import com.aptech.pet_adoption.animal.service.AnimalService;
import com.aptech.pet_adoption.animal.payload.request.CreateAnimal;
import com.aptech.pet_adoption.utils.models.Response;
import com.aptech.pet_adoption.utils.service.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/animal")
public class AnimalController {
    @Autowired
    UtilsService utilsService;

    @Autowired
    AnimalService service;

    @GetMapping("/all")
    public ResponseEntity<Response> getAll(){
        try {
            Response response = service.getAll();
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getOne(@PathVariable("id") long id){
        try {
            Response response = service.getOne(id);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }


    @PostMapping("/create")
    public ResponseEntity<Response> create(@RequestBody CreateAnimal dto){
        try {
            Response response = service.create(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/update")
    public ResponseEntity<Response> update(@RequestBody UpdateAnimal dto){
        try {
            Response response = service.update(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<Response> delete(@RequestBody DeleteAnimal dto){
        try {
            Response response = service.delete(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }
}
