package com.aptech.pet_adoption.animal.payload.request;

import lombok.Data;

@Data
public class UpdateAnimal {
    private long id;

    private String name;

}
