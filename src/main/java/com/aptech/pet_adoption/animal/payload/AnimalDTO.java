package com.aptech.pet_adoption.animal.payload;

import lombok.Data;

@Data
public class AnimalDTO {
    private long id;

    private String name;

}
