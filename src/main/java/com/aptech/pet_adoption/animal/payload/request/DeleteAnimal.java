package com.aptech.pet_adoption.animal.payload.request;

import lombok.Data;

@Data
public class DeleteAnimal {
    private long id;
}
