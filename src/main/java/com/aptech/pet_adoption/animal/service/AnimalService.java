package com.aptech.pet_adoption.animal.service;


import com.aptech.pet_adoption.animal.models.Animal;
import com.aptech.pet_adoption.animal.payload.AnimalDTO;
import com.aptech.pet_adoption.animal.payload.request.DeleteAnimal;
import com.aptech.pet_adoption.animal.payload.request.UpdateAnimal;
import com.aptech.pet_adoption.animal.models.repository.AnimalRepository;
import com.aptech.pet_adoption.animal.payload.request.CreateAnimal;
import com.aptech.pet_adoption.utils.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnimalService {
    @Autowired
    AnimalRepository animalRepository;


    public Response getAll(){
        Response response = new Response();
        try {
            List<Animal> animals = animalRepository.findAll();
            List<AnimalDTO> animalDTOS = new ArrayList<>();
            for (Animal animal : animals){
                AnimalDTO dto = new AnimalDTO();
                dto.setId(animal.getId());
                dto.setName(animal.getName());
                animalDTOS.add(dto);
            }
            response.setStatus(200);
            response.setMassage("Find All Successful");
            response.setData(animalDTOS);
            return response;
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response getOne(long id){
        Response response = new Response();
        try {
            Animal animal = animalRepository.findById(id).orElse(null);
            if(animal == null){
                AnimalDTO dto = new AnimalDTO();
                dto.setId(animal.getId());
                dto.setName(animal.getName());
                response.setStatus(200);
                response.setMassage("Find Successful");
                response.setData(dto);
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Category does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }


    public Response create(CreateAnimal dto){
        Response response = new Response();
        try {
            //validate sau
            Animal animal = animalRepository.findByName(dto.getName()).orElse(null);
            if(animal == null){
                animal = new Animal();
                animal.setName(dto.getName());
                animalRepository.save(animal);
                response.setStatus(200);
                response.setMassage("Created");
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Category already exists");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response update(UpdateAnimal dto){
        Response response = new Response();
        try {
            //validate sau
            Animal animal = animalRepository.findById(dto.getId()).orElse(null);
            if (animal != null){
                Animal check = animalRepository.findByName(dto.getName()).orElse(null);
                if(check == null){
                    animal.setName(dto.getName());
                    animalRepository.save(animal);
                    response.setStatus(200);
                    response.setMassage("Updated");
                    return response;
                }else {
                    response.setStatus(400);
                    response.setMassage("Category already exists");
                    return response;
                }
            }else {
                response.setStatus(400);
                response.setMassage("Category does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response delete(DeleteAnimal dto){
        Response response = new Response();
        try {
            //Validate sau
            Animal animal = animalRepository.findById(dto.getId()).orElse(null);
            if (animal != null){
                animal.setId(dto.getId());
                animalRepository.delete(animal);
                response.setStatus(200);
                response.setMassage("Deleted");
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Category does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }
}
