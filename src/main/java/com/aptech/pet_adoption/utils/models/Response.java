package com.aptech.pet_adoption.utils.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Response {
    private int status;

    private String massage;

    private Object data;

    private List<String> Error = new ArrayList<>();
}
