package com.aptech.pet_adoption.utils.service;

import com.aptech.pet_adoption.utils.models.Response;
import org.springframework.stereotype.Service;

@Service
public class UtilsService {

    public Response responseError(){
        Response response = new Response();
        try {
                response.setData(400);
                response.setMassage("Contact to admin for more information!");
                return response;
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

}
