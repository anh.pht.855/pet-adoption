package com.aptech.pet_adoption.authentication.services;



import com.aptech.pet_adoption.authentication.models.Role;
import com.aptech.pet_adoption.authentication.models.User;
import com.aptech.pet_adoption.authentication.models.repository.RoleRepository;
import com.aptech.pet_adoption.authentication.models.repository.UserRepository;
import com.aptech.pet_adoption.authentication.payload.request.ChangePasswordRequest;
import com.aptech.pet_adoption.authentication.payload.request.LoginRequest;
import com.aptech.pet_adoption.authentication.payload.response.JwtResponse;
import com.aptech.pet_adoption.authentication.security.jwt.JwtUtils;
import com.aptech.pet_adoption.authentication.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    public UserRepository userRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private PasswordEncoder encoder;


    @Autowired
    private RoleRepository roleRepository;

    public JwtResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                roles);
    }

    public void changePassword(ChangePasswordRequest cpr) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(cpr.getEmail(), cpr.getCurrentPassword()));

        if (!authentication.isAuthenticated())
            throw new RuntimeException("Can not authorize user to reset password!");

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user = userRepository.findById(userDetails.getId())
                .orElseThrow(() -> new RuntimeException("User not found!"));

        user.setPassword(encoder.encode(cpr.getNewPassword()));
        userRepository.save(user);
    }

    public User getUserByToken(String token) {
        String username = jwtUtils.getUserNameFromJwtToken(token);
        return userRepository.findByEmail(username)
                .orElseThrow(() -> new RuntimeException("User not found!"));
    }

    public void resetUserPassword(User user, String password) {
        user.setPassword(encoder.encode(password));
        userRepository.save(user);
    }

    public User registerUser(String username, String email, String password, long role) {
        if (userRepository.existsByEmail(username))
            throw new RuntimeException(String.format("Username %s has exists!", username));

        Role userRole = roleRepository.findById(role)
                .orElseThrow(() -> new RuntimeException(String.format("Role %s not found!", role)));

        User user = new User();

        user.getRoles().add(userRole);

        return userRepository.save(user);
    }

    public void deleteUser(User user) {
        userRepository.deleteById(user.getId());
    }
}
