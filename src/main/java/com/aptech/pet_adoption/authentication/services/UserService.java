package com.aptech.pet_adoption.authentication.services;

import com.aptech.pet_adoption.authentication.models.Role;
import com.aptech.pet_adoption.authentication.models.User;
import com.aptech.pet_adoption.authentication.models.repository.UserRepository;
import com.aptech.pet_adoption.authentication.payload.UserDTO;
import com.aptech.pet_adoption.helpers.date.DateTimeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class UserService {
    @Autowired
    public UserRepository userRepository;

    public UserDTO getById(Long id) {
        User user = this.userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("User with id %d not found!", id)));

        return bindUserData(user);
    }

    public List<UserDTO> getAll() {
        List<User> userList = this.userRepository.findAll();
        List<UserDTO> result = new ArrayList<>();

        for (User user: userList) {
            result.add(bindUserData(user));
        }

        return result;
    }

    public UserDTO bindUserData(User user) {
        Iterator<Role> iter = user.getRoles().iterator();

        UserDTO userDTO  =  new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(iter.next().getName());
        userDTO.setAddress(user.getAddress());
        userDTO.setPhone(user.getPhone());
        return userDTO;

    }
}
