package com.aptech.pet_adoption.authentication.controller;

import javax.validation.Valid;


import com.aptech.pet_adoption.authentication.payload.request.ChangePasswordRequest;
import com.aptech.pet_adoption.authentication.payload.request.LoginRequest;
import com.aptech.pet_adoption.authentication.payload.response.JwtResponse;
import com.aptech.pet_adoption.authentication.services.AuthService;
import com.aptech.pet_adoption.helpers.erorrs.ErrorResponse;
import com.aptech.pet_adoption.helpers.message.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/v1/auth")
public class AuthController {
    @Autowired
    public AuthService userService;
    @PreAuthorize("permitAll()")
    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        try {
            JwtResponse jwtResponse = userService.login(loginRequest);
            return ResponseEntity.ok(jwtResponse);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ErrorResponse(
                    400,
                    e.getMessage(),
                    "Contact to admin for more information!"
            ));
        }
    }



    @PostMapping("/password/change")
    public ResponseEntity<?> resetPassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        try {
            userService.changePassword(changePasswordRequest);
            return ResponseEntity.ok(new MessageResponse("Update password success"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ErrorResponse(
                    400,
                    e.getMessage(),
                    "Contact to admin for more information!")
            );
        }
    }
}
