package com.aptech.pet_adoption.authentication.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
@Data
public class LoginRequest {
    @NotNull
    @NotBlank(message = "Email can not empty!")
    private String email;

    @NotNull
    @NotBlank(message = "Password can not empty!")
    private String password;
}
