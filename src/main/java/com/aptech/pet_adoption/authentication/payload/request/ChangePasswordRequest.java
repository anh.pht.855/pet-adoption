package com.aptech.pet_adoption.authentication.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ChangePasswordRequest {
    @NotNull
    @NotBlank(message = "email can not empty!")
    private String email;

    @NotNull
    @NotBlank(message = "Current password can not empty!")
    private String currentPassword;

    @NotNull
    @NotBlank(message = "New password can not empty!")
    private String newPassword;


}
