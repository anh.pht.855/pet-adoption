package com.aptech.pet_adoption.authentication.payload;

import lombok.Data;

import javax.persistence.Column;

@Data
public class UserDTO {
    private Long id;

    private String email;

    private String role;
    
    private String address;

    private String phone;
}
