package com.aptech.pet_adoption.category.controller;

import com.aptech.pet_adoption.category.payload.request.CreateCategory;
import com.aptech.pet_adoption.category.payload.request.DeleteCategory;
import com.aptech.pet_adoption.category.payload.request.UpdateCategory;
import com.aptech.pet_adoption.category.service.CategoryService;
import com.aptech.pet_adoption.utils.models.Response;
import com.aptech.pet_adoption.utils.service.UtilsService;
import org.hibernate.query.criteria.internal.expression.function.UpperFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/category")
public class CategoryController {
    @Autowired
    UtilsService utilsService;

    @Autowired
    CategoryService service;

    @GetMapping("/all")
    public ResponseEntity<Response> getAll(){
        try {
            Response response = service.getAll();
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getOne(@PathVariable("id") long id){
        try {
            Response response = service.getOne(id);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }


    @PostMapping("/create")
    public ResponseEntity<Response> create(@RequestBody CreateCategory dto){
        try {
            Response response = service.create(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/update")
    public ResponseEntity<Response> update(@RequestBody UpdateCategory dto){
        try {
            Response response = service.update(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<Response> delete(@RequestBody DeleteCategory deleteCategory){
        try {
            Response response = service.delete(deleteCategory);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }
}
