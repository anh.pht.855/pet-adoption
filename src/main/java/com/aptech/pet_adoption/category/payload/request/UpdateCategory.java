package com.aptech.pet_adoption.category.payload.request;

import lombok.Data;

@Data
public class UpdateCategory {
    private long id;

    private String name;

}
