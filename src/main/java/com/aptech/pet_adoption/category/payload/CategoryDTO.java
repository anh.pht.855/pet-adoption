package com.aptech.pet_adoption.category.payload;

import lombok.Data;

@Data
public class CategoryDTO {
    private long id;

    private String name;

}
