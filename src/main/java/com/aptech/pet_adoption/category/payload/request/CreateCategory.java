package com.aptech.pet_adoption.category.payload.request;

import lombok.Data;

@Data
public class CreateCategory {
    private String name;
}
