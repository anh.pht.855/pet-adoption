package com.aptech.pet_adoption.category.service;

import com.aptech.pet_adoption.category.models.Category;
import com.aptech.pet_adoption.category.models.repository.CategoryRepository;
import com.aptech.pet_adoption.category.payload.CategoryDTO;
import com.aptech.pet_adoption.category.payload.request.CreateCategory;
import com.aptech.pet_adoption.category.payload.request.DeleteCategory;
import com.aptech.pet_adoption.category.payload.request.UpdateCategory;
import com.aptech.pet_adoption.utils.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository categoryRepository;


    public Response getAll(){
        Response response = new Response();
        try {
            List<Category> categories = categoryRepository.findAll();
            List<CategoryDTO> categoryDTOS = new ArrayList<>();
            for (Category category : categories){
                CategoryDTO dto = new CategoryDTO();
                dto.setId(category.getId());
                dto.setName(category.getName());
                categoryDTOS.add(dto);
            }
            response.setStatus(200);
            response.setMassage("Find All Successful");
            response.setData(categoryDTOS);
            return response;
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response getOne(long id){
        Response response = new Response();
        try {
            Category category = categoryRepository.findById(id).orElse(null);
            if(category == null){
                CategoryDTO dto = new CategoryDTO();
                dto.setId(category.getId());
                dto.setName(category.getName());
                response.setStatus(200);
                response.setMassage("Find Successful");
                response.setData(dto);
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Category does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }


    public Response create(CreateCategory createCategory){
        Response response = new Response();
        try {
            //validate sau
            Category category = categoryRepository.findByName(createCategory.getName()).orElse(null);
            if(category == null){
                category = new Category();
                category.setName(createCategory.getName());
                categoryRepository.save(category);
                response.setStatus(200);
                response.setMassage("Created");
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Category already exists");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response update(UpdateCategory dto){
        Response response = new Response();
        try {
            //validate sau
            Category category = categoryRepository.findById(dto.getId()).orElse(null);
            if (category != null){
                Category check = categoryRepository.findByName(dto.getName()).orElse(null);
                if(check == null){
                    category.setName(dto.getName());
                    categoryRepository.save(category);
                    response.setStatus(200);
                    response.setMassage("Updated");
                    return response;
                }else {
                    response.setStatus(400);
                    response.setMassage("Category already exists");
                    return response;
                }
            }else {
                response.setStatus(400);
                response.setMassage("Category does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response delete(DeleteCategory dto){
        Response response = new Response();
        try {
            //Validate sau
            Category category = categoryRepository.findById(dto.getId()).orElse(null);
            if (category != null){
                category.setId(dto.getId());
                categoryRepository.delete(category);
                response.setStatus(200);
                response.setMassage("Deleted");
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Category does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }
}
