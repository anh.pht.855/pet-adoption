package com.aptech.pet_adoption.shopping.models.repository;

import com.aptech.pet_adoption.authentication.models.User;
import com.aptech.pet_adoption.shopping.models.Shopping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShoppingRepository extends JpaRepository<Shopping,Long> {
//    Optional<Shopping> findByUses(User user);
}
