package com.aptech.pet_adoption.shopping.models.repository;

import com.aptech.pet_adoption.shopping.models.ItemCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemCartRepository extends JpaRepository<ItemCart,Long> {
}
