package com.aptech.pet_adoption.shopping.models;

import com.aptech.pet_adoption.product.models.Product;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "item_cart")
public class ItemCart {
    @EmbeddedId
    private Pk pk;

    @Embeddable
    public static class Pk implements Serializable {
        private static final long serialVersionUID = 1L;

        @ManyToOne
        @JoinColumn(name = "cart_id")
        private Shopping shopping;

        @ManyToOne
        @JoinColumn(name = "product_id")
        private Product product;
    }
    @Column(name = "quantity")
    private int quantity;
}
