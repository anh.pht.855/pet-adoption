package com.aptech.pet_adoption.shopping.payload.request;

import lombok.Data;

@Data
public class addItem {
    private long userId;

    private long productId;

    private int quantity;
}
