package com.aptech.pet_adoption.shopping.payload;

import lombok.Data;

@Data
public class ItemCartDTO {
    private long id;

    private String product_name;

    private int quantity;

}
