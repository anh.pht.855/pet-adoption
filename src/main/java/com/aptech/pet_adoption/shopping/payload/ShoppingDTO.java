package com.aptech.pet_adoption.shopping.payload;

import com.aptech.pet_adoption.shopping.payload.response.ItemDTO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ShoppingDTO {
    private long id;

    private String email;

    private String userName;

    private List<ItemDTO> itemCarts = new ArrayList<>();
}
