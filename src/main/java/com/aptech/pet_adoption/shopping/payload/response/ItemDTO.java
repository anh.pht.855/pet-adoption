package com.aptech.pet_adoption.shopping.payload.response;

import lombok.Data;

@Data
public class ItemDTO {
    private String productName;

    private int quantity;
}
