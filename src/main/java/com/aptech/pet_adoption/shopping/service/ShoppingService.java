package com.aptech.pet_adoption.shopping.service;

import com.aptech.pet_adoption.authentication.models.User;
import com.aptech.pet_adoption.authentication.models.repository.UserRepository;
import com.aptech.pet_adoption.shopping.models.ItemCart;
import com.aptech.pet_adoption.shopping.models.Shopping;
import com.aptech.pet_adoption.shopping.models.repository.ItemCartRepository;
import com.aptech.pet_adoption.shopping.models.repository.ShoppingRepository;
import com.aptech.pet_adoption.shopping.payload.ShoppingDTO;
import com.aptech.pet_adoption.shopping.payload.request.addItem;
import com.aptech.pet_adoption.shopping.payload.response.ItemDTO;
import com.aptech.pet_adoption.utils.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingService {

    @Autowired
    ItemCartRepository itemCartRepository;

    @Autowired
    ShoppingRepository shoppingRepository;

    @Autowired
    UserRepository userRepository;

//    public Response shoppingByUser(long user_id){
//        Response response = new Response();
//        try {
//            User user = userRepository.findById(user_id).orElse(null);
//            if(user == null){
//                response.setStatus(400);
//                response.setMassage("User does not exist");
//                return response;
//            }
//            Shopping shopping = shoppingRepository.findByUses(user).orElse(null);
//            if(shopping == null){
//                response.setStatus(400);
//                response.setMassage("Shopping does not exist");
//                return response;
//            }
//            List<ItemCart> itemCarts = itemCartRepository.findAllByShopping(shopping.getId());
//            ShoppingDTO shoppingDTO = new ShoppingDTO();
//            shoppingDTO.setUserName(shopping.getUses_id().getUserName());
//            shoppingDTO.setEmail(shopping.getUses_id().getEmail());
//            for (ItemCart itemCart : itemCarts){
//                ItemDTO itemDTO = new ItemDTO();
//                itemDTO.setQuantity(itemCart.getQuantity());
//                itemDTO.setProductName(itemCart.getProduct().getName());
//                shoppingDTO.getItemCarts().add(itemDTO);
//            }
//            response.setStatus(200);
//            response.setMassage("Find All Successful");
//            response.setData(shoppingDTO);
//            return response;
//        }catch (Exception e){
//            response.setStatus(500);
//            response.setMassage("Contact to admin for more information!");
//            response.getError().add(e.getMessage());
//            return response;
//        }
//    }

//    public Response addItem(addItem dto){
//        Response response = new Response();
//        try{
//
//
//        }catch (Exception e){
//            response.setStatus(500);
//            response.setMassage("Contact to admin for more information!");
//            response.getError().add(e.getMessage());
//            return response;
//        }
//    }
}
