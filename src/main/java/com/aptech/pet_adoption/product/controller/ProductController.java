package com.aptech.pet_adoption.product.controller;

import com.aptech.pet_adoption.product.payload.request.CreateProduct;
import com.aptech.pet_adoption.product.payload.request.DeleteProduct;
import com.aptech.pet_adoption.product.payload.request.UpdateProduct;
import com.aptech.pet_adoption.product.service.ProductService;
import com.aptech.pet_adoption.utils.models.Response;
import com.aptech.pet_adoption.utils.service.UtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/product")
public class ProductController {
    @Autowired
    UtilsService utilsService;

    @Autowired
    ProductService productService;

    @GetMapping("/all")
    public ResponseEntity<Response> getAll(){
        try {
            Response response = productService.getAll();
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getOne(@PathVariable("id") long id){
        try {
            Response response = productService.getOne(id);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Response> create(@RequestBody CreateProduct dto){
        try {
            Response response = productService.create(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/update")
    public ResponseEntity<Response> update(@RequestBody UpdateProduct dto){
        try {
            Response response = productService.update(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<Response> delete(@RequestBody DeleteProduct dto){
        try {
            Response response = productService.delete(dto);
            return ResponseEntity.status(response.getStatus()).body(response);
        }catch (Exception e){
            return ResponseEntity.status(utilsService.responseError().getStatus()).body(utilsService.responseError());
        }
    }
}
