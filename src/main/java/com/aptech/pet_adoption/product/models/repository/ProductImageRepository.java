package com.aptech.pet_adoption.product.models.repository;

import com.aptech.pet_adoption.product.models.Product;
import com.aptech.pet_adoption.product.models.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductImageRepository extends JpaRepository<ProductImage,Long> {

    List<ProductImage> findAllByProduct(Product product);
}
