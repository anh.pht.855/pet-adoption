package com.aptech.pet_adoption.product.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "product_image")
public class ProductImage {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;

    @Column(name = "url")
    private String url;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
}
