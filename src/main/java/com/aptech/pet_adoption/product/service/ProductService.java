package com.aptech.pet_adoption.product.service;

import com.aptech.pet_adoption.category.models.Category;
import com.aptech.pet_adoption.category.models.repository.CategoryRepository;
import com.aptech.pet_adoption.product.models.Product;
import com.aptech.pet_adoption.product.models.ProductImage;
import com.aptech.pet_adoption.product.models.repository.ProductImageRepository;
import com.aptech.pet_adoption.product.models.repository.ProductRepository;
import com.aptech.pet_adoption.product.payload.ProductDTO;
import com.aptech.pet_adoption.product.payload.request.CreateProduct;
import com.aptech.pet_adoption.product.payload.request.DeleteProduct;
import com.aptech.pet_adoption.product.payload.request.UpdateProduct;
import com.aptech.pet_adoption.utils.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductImageRepository productImageRepository;

    @Autowired
    CategoryRepository categoryRepository;


    public Response getAll(){
        Response response = new Response();
        try {
            List<Product> products = productRepository.findAll();
            List<ProductDTO> dto = new ArrayList<>();
            for (Product product : products){
                ProductDTO  productDTO = new ProductDTO();
                List<ProductImage> productImages = productImageRepository.findAllByProduct(product);
                productDTO.setId(product.getId());
                productDTO.setName(product.getName());
                productDTO.setDescription(product.getDescription());
                productDTO.setCategoryName(product.getCategory().getName());
                for(ProductImage image : productImages){
                    productDTO.getImage().add(image.getUrl());
                }
                dto.add(productDTO);
            }
            response.setStatus(200);
            response.setMassage("Find All Successful");
            response.setData(dto);
            return response;
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response getOne(long id){
        Response response = new Response();
        try {
            Product product = productRepository.findById(id).orElse(null);
            if(product != null){
                    ProductDTO  productDTO = new ProductDTO();
                    List<ProductImage> productImages = productImageRepository.findAllByProduct(product);
                    productDTO.setId(product.getId());
                    productDTO.setName(product.getName());
                    productDTO.setDescription(product.getDescription());
                    productDTO.setCategoryName(product.getCategory().getName());
                    for(ProductImage image : productImages){
                        productDTO.getImage().add(image.getUrl());
                    }
                response.setStatus(200);
                response.setMassage("Find All Successful");
                response.setData(productDTO);
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Product does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response create(CreateProduct dto){
        Response response = new Response();
        try {
            //validate sau
            Product product = productRepository.findByName(dto.getName()).orElse(null);
            if(product == null){
                product = new Product();
                Category category = categoryRepository.findById(dto.getCategory_id()).orElse(null);
                if(category == null){
                    response.setStatus(400);
                    response.setMassage("Category does not exist");
                    return response;
                }
                product.setName(dto.getName());
                product.setDescription(dto.getDescription());
                product.setCategory(category);
                product.setPrice(dto.getPrice());
                productRepository.save(product);
                response.setStatus(200);
                response.setMassage("create");
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Product already exists");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response update(UpdateProduct dto){
        Response response = new Response();
        try {
            //validate sau
            Product product = productRepository.findById(dto.getId()).orElse(null);
            if(product != null){
                Product check = productRepository.findByName(dto.getName()).orElse(null);
                if(check == null){
                    Category category = categoryRepository.findById(dto.getCategory_id()).orElse(null);
                    if(category != null){
                        product.setName(dto.getName());
                        product.setDescription(dto.getDescription());
                        product.setCategory(category);
                        product.setPrice(dto.getPrice());
                        productRepository.save(product);
                        response.setStatus(200);
                        response.setMassage("create");
                        return response;
                    }else {
                        response.setStatus(400);
                        response.setMassage("Category does not exist");
                        return response;
                    }
                }else {
                    response.setStatus(400);
                    response.setMassage("Product already exists");
                    return response;
                }
            }else {
                response.setStatus(400);
                response.setMassage("Product does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public Response delete(DeleteProduct dto){
        Response response = new Response();
        try {
            //validate sau
            Product product = productRepository.findById(dto.getId()).orElse(null);
            if (product != null){
                productRepository.delete(product);
                response.setStatus(200);
                response.setMassage("deleted");
                return response;
            }else {
                response.setStatus(400);
                response.setMassage("Product does not exist");
                return response;
            }
        }catch (Exception e){
            response.setStatus(500);
            response.setMassage("Contact to admin for more information!");
            response.getError().add(e.getMessage());
            return response;
        }
    }

    public void addImage(long id, MultipartFile[] multipartFiles){


    }

    public void deleteImage(String url){

    }
}
