package com.aptech.pet_adoption.product.payload;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductDTO {
    private long id;

    private String name;

    private float price;

    private String description;

    private String categoryName;

    private List<String> image = new ArrayList<>();

}
