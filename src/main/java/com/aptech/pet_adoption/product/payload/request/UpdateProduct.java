package com.aptech.pet_adoption.product.payload.request;

import lombok.Data;

@Data
public class UpdateProduct {

    private long id;

    private String name;

    private String description;

    private float price;

    private long category_id;
}
