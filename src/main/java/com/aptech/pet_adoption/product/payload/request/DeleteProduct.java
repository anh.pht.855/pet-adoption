package com.aptech.pet_adoption.product.payload.request;

import lombok.Data;

@Data
public class DeleteProduct {
    private long id;
}
