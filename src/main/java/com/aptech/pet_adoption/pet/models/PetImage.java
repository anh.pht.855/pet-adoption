package com.aptech.pet_adoption.pet.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pet_image")
public class PetImage {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;

    @Column(name = "url")
    private String url;
}
