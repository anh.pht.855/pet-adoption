package com.aptech.pet_adoption.pet.models;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "information")
public class Information {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;


}
