package com.aptech.pet_adoption.pet.models;

import com.aptech.pet_adoption.animal.models.Animal;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "pet")
public class Pet {
    @Id
    @Column(name = "pet_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "pet_name")
    private String name;

    @Column(name = "age")
    private int age;

    @Column(name = "color")
    private String color;

    @Column(name = "weight")
    private float weight;

    @Column(name = "about")
    private String about;

    @Column(name = "status")
    private boolean status;

    @Column(name = "adoption")
    private boolean adoption;

    @Column(name = "gender")
    private boolean gender;

    @ManyToOne
    @JoinColumn(name = "animal_id")
    private Animal animal;


    @OneToMany
    private Set<PetImage> images;

    @ManyToMany
    @JoinTable(name = "pet_information"
            ,joinColumns = @JoinColumn(name = "pet_id")
            ,inverseJoinColumns = @JoinColumn(name = "information_id"))
    private Set<Information> information;
}
